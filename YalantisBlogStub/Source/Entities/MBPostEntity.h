//
//  MBPostEntity.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBPostEntity : NSObject
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *postId;
@property (nonatomic, readonly) NSString *postDescription;
@property (nonatomic, readonly) NSString *author;
@property (nonatomic, readonly) NSURL    *postURL;
@property (nonatomic, readonly) NSURL    *imageURL;

@property (nonatomic, readonly) NSUInteger visitorsCount;


- (instancetype)initWithId:(NSString *)postId
                     title:(NSString *)title
           postDescription:(NSString *)postDescription
                    author:(NSString *)author
             visitorsCount:(NSUInteger)visitorsCount
                   postURL:(NSURL *)postURL
                  imageURL:(NSURL *)imageURL;

@end
