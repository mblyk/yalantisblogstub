//
//  MBPostEntity.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBPostEntity.h"

@implementation MBPostEntity

#pragma mark -
#pragma mark Initializations

- (instancetype)initWithId:(NSString *)postId
                     title:(NSString *)title
           postDescription:(NSString *)postDescription
                    author:(NSString *)author
             visitorsCount:(NSUInteger)visitorsCount
                   postURL:(NSURL *)postURL
                  imageURL:(NSURL *)imageURL
{
    self = [super init];
    if (self) {
        _postId  = postId;
        _title = title;
        _postDescription = postDescription;
        _author = author;
        _visitorsCount = visitorsCount;
        _postURL = postURL;
        _imageURL = imageURL;
    }
    
    return self;
}

@end
