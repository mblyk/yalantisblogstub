//
//  MBAsynchronousOperation.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 23.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBAsynchronousOperation : NSOperation

- (void)completeOperation;

@end
