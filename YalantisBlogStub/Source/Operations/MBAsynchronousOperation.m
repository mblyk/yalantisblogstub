//
//  MBAsynchronousOperation.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 23.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBAsynchronousOperation.h"

static NSString * const  MBFinishedKey = @"isFinished";
static NSString * const  MBExecutingKey = @"isExecuting";

@interface MBAsynchronousOperation () {
    BOOL        executing;
    BOOL        finished;
}

@end

@implementation MBAsynchronousOperation

- (void)start {
    if (self.isCancelled) {
        [self willChangeValueForKey:MBFinishedKey];
        finished = YES;
        [self didChangeValueForKey:MBFinishedKey];
        return;
    }
    
    [self willChangeValueForKey:MBExecutingKey];
    executing = YES;
    [self didChangeValueForKey:MBExecutingKey];
    [self main];
}


- (BOOL)isExecuting {
    return executing;
}

- (BOOL)isFinished{
    return finished;
}

- (BOOL)isAsynchronous {
    return YES;
}

- (void)completeOperation {
    [self willChangeValueForKey:MBFinishedKey];
    [self willChangeValueForKey:MBExecutingKey];
    
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:MBExecutingKey];
    [self didChangeValueForKey:MBFinishedKey];
}

@end
