//
//  MBLoadPostsOperation.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBAsynchronousOperation.h"

@protocol MBLoadPostsOperationDelegate <NSObject>
- (void)didLoadPosts:(NSArray *)posts isNext:(BOOL)isNext;
- (void)didFailToLoadPostsWithIsNext:(BOOL)isNext;

@end

@interface MBLoadPostsOperation : MBAsynchronousOperation
@property (nonatomic, weak) id<MBLoadPostsOperationDelegate> delegate;

- (instancetype)initWithIsNext:(BOOL)isNext;

@end
