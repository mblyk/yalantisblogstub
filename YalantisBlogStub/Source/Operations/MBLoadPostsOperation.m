//
//  MBLoadPostsOperation.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBLoadPostsOperation.h"
#import "MBPostEntity.h"

static NSString * const MBURL = @"https://yalantis.com/blog/";

static NSString * const MBIdKey            = @"id";
static NSString * const MBImageURLKey      = @"image_url";
static NSString * const MBItemsKey         = @"items";
static NSString * const MBDescriptionKey   = @"description";
static NSString * const MBTitleKey         = @"title";
static NSString * const MBAuthorKey        = @"author";
static NSString * const MBPostURLKey       = @"post_url";
static NSString * const MBResponseKey      = @"response";
static NSString * const MBVisitorsCountKey = @"visitors_count";

@interface MBLoadPostsOperation ()
@property (nonatomic, assign)   BOOL isNext;

//private methods
- (void)processResponse:(NSData *)data;

@end


@implementation MBLoadPostsOperation

#pragma mark -
#pragma mark Initializations

- (instancetype)initWithIsNext:(BOOL)isNext {
    self = [super init];
    if (self) {
        self.isNext = isNext;
    }
    
    return self;
}

#pragma mark -
#pragma mark Public

- (void)main {
    if (self.isCancelled) {
        [self completeOperation];
        return;
    }
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject];
    
    NSURL * url = [NSURL URLWithString:MBURL];
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithURL:url
                                                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (!error) {
                                              [self processResponse:data];
                                          } else {
                                              [self.delegate didFailToLoadPostsWithIsNext:self.isNext];
                                              [self completeOperation];
                                          }
                                          
                                          [defaultSession finishTasksAndInvalidate];
                                      }];
    [dataTask resume];
    
    if (self.isCancelled) {
        [self completeOperation];
        return;
    }
}

#pragma mark -
#pragma mark Private

- (void)processResponse:(NSData *)data {
    NSError* serializationError;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableLeaves
                                                           error:&serializationError];
    if (serializationError) {
        [self.delegate didFailToLoadPostsWithIsNext:self.isNext];
        [self completeOperation];
    } else {
        NSArray *posts = [[json objectForKey:MBResponseKey] objectForKey:MBItemsKey];
        NSMutableArray *result = [NSMutableArray array];
        
        for (NSDictionary *post in posts) {
            NSString *postId = [post objectForKey:MBIdKey];
            NSString *author =[post objectForKey:MBAuthorKey];
            NSString *title = [post objectForKey:MBTitleKey];
            NSString *despription = [post objectForKey:MBDescriptionKey];
            NSURL *postURL = [NSURL URLWithString:[post objectForKey:MBPostURLKey]];
            NSURL *imageURL = [NSURL URLWithString:[post objectForKey:MBImageURLKey]];
            NSNumber *visitorsCount = [post objectForKey:MBVisitorsCountKey];
            MBPostEntity *entity = [[MBPostEntity alloc] initWithId:postId
                                                              title:title
                                                    postDescription:despription
                                                             author:author
                                                      visitorsCount:[visitorsCount integerValue]
                                                            postURL:postURL
                                                           imageURL:imageURL];
            
            [result addObject:entity];
        }
        
        [self.delegate didLoadPosts:result isNext:self.isNext];
        [self completeOperation];
    }
}

@end
