//
//  MBSetImageOnCellOperation.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 22.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBAsynchronousOperation.h"

@class MBBlogPostTableViewCell;

@interface MBSetImageOnCellOperation : MBAsynchronousOperation
@property (nonatomic, readonly)   NSIndexPath *indexPath;

- (instancetype)initWithCell:(MBBlogPostTableViewCell *)cell
                    imageURL:(NSURL *)imageURL
                   indexPath:(NSIndexPath *)indexPath;

@end
