//
//  MBSetImageOnCellOperation.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 22.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBSetImageOnCellOperation.h"
#import <SDWebImage/SDWebImageManager.h>
#import "MBBlogPostTableViewCell.h"

static NSString * const MBURL = @"https://yalantis.com/blog/";

@interface MBSetImageOnCellOperation ()
@property (nonatomic, strong)   id<SDWebImageOperation> operation;
@property (nonatomic, strong)   NSURL *imageURL;
@property (nonatomic, strong)   MBBlogPostTableViewCell *cell;

@end

@implementation MBSetImageOnCellOperation

#pragma mark -
#pragma mark Initializations

- (instancetype)initWithCell:(MBBlogPostTableViewCell *)cell imageURL:(NSURL *)imageURL indexPath:(NSIndexPath *)indexPath {
    self = [super init];
    if (self) {
        self.imageURL = imageURL;
        self.cell = cell;
        _indexPath = indexPath;
    }
    
    return self;
}

#pragma mark -
#pragma mark Public

- (void)cancel {
    [self.operation cancel];
}

- (void)main {
    if (self.isCancelled) {
        [self completeOperation];
        return;
    }
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    __weak typeof (self) weakSelf = self;
    
    self.operation = [manager downloadImageWithURL:self.imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             
                         } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                             if (self.isCancelled) {
                                 [weakSelf completeOperation];
                                 return ;
                             }
                            
                             if (image) {
                                 [weakSelf.cell setupImage:image];
                             }
                             
                             [weakSelf completeOperation];
                         }];
    
}

@end
