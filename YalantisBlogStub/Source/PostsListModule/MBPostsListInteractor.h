//
//  MBPostsListInteractor.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>

@class MBPostsListPresenter;
@class MBPostEntity;
@class MBBlogPostTableViewCell;

@interface MBPostsListInteractor : NSObject
@property (nonatomic, weak) MBPostsListPresenter *presenter;

- (void)fetchPosts;
- (void)fetchNextPosts;

- (void)setupCell:(MBBlogPostTableViewCell *)cell
      atIndexPath:(NSIndexPath *)indexPath;

- (void)disableImageLoading;
- (void)enableImageLoadingWithOnScreenIndexPaths:(NSArray<NSIndexPath *> *)indexPaths;

- (MBPostEntity *)postEntityAtIndexPath:(NSIndexPath *)indexPath;
- (NSUInteger)numberOfPosts;

- (void)clearMemory;

@end
