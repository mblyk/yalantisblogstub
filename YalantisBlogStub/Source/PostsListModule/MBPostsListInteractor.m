//
//  MBPostsListInteractor.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBPostsListInteractor.h"
#import "MBLoadPostsOperation.h"
#import "MBPostsListPresenter.h"
#import "MBBlogPostTableViewCell.h"
#import "MBSetImageOnCellOperation.h"
#import "MBPostEntity.h"
#import <SDWebImage/SDImageCache.h>
#import "MBReachabilityManager.h"

@interface MBPostsListInteractor () <MBLoadPostsOperationDelegate>
@property (nonatomic, strong)   NSOperationQueue    *queue;
@property (nonatomic, strong)   MBLoadPostsOperation    *operation;
@property (nonatomic, strong)   NSMutableArray<MBPostEntity *> *posts;

@property (nonatomic, strong)   NSMutableDictionary *operationsInProgress;

@property (nonatomic, strong)   MBReachabilityManager *reach;

@end

@implementation MBPostsListInteractor

#pragma mark -
#pragma mark Initializations

- (instancetype)init {
    self = [super init];
    if (self) {
        self.queue = [[NSOperationQueue alloc] init];
        self.operationsInProgress = [[NSMutableDictionary alloc] init];
        self.reach = [MBReachabilityManager sharedManager];
    }
    
    return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)disableImageLoading {
    self.queue.suspended = YES;
}

- (void)enableImageLoadingWithOnScreenIndexPaths:(NSArray<NSIndexPath *> *)indexPaths {
     [self.operationsInProgress allKeys];
     NSMutableSet<NSIndexPath *> *allOperations = [NSMutableSet setWithArray:[self.operationsInProgress allKeys]];
    NSMutableSet *operationsOnScreen = [NSMutableSet setWithArray:indexPaths];
    for (NSIndexPath *indexPath in allOperations) {
        if (![operationsOnScreen containsObject:indexPath]) {
            MBSetImageOnCellOperation *operation = [self.operationsInProgress objectForKey:indexPath];
            [operation cancel];
            [self.operationsInProgress removeObjectForKey:indexPath];
        }
    }
    
    self.queue.suspended = NO;
}

- (void)fetchNextPosts {
    self.operation = [[MBLoadPostsOperation alloc] initWithIsNext:YES];
    self.operation.delegate = self;
    [self.queue addOperation:self.operation];
}

- (void)fetchPosts {
    self.operation = [[MBLoadPostsOperation alloc] initWithIsNext:NO];
    self.operation.delegate = self;
    [self.queue addOperation:self.operation];
    
}

- (void)setupCell:(MBBlogPostTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    MBPostEntity *entity = [self postEntityAtIndexPath:indexPath];
    [cell setupWithPostEntity:entity];
    MBSetImageOnCellOperation *operation = [[MBSetImageOnCellOperation alloc] initWithCell:cell
                                                                                  imageURL:entity.imageURL
                                                                                 indexPath:indexPath];
    
    [self.operationsInProgress setObject:operation forKey:indexPath];
    
    __weak NSOperation *weakOperation = operation;
    operation.completionBlock = ^{
        if (weakOperation.isCancelled) {
            return ;
        }
        
        [self.operationsInProgress removeObjectForKey:indexPath];
    };
    
    [self.queue addOperation:operation];
}

- (MBPostEntity *)postEntityAtIndexPath:(NSIndexPath *)indexPath {
    return self.posts[indexPath.row];
}

- (NSUInteger)numberOfPosts {
    return self.posts.count;
}

- (void)clearMemory {
    SDImageCache *cache = [SDImageCache sharedImageCache];
    [cache clearMemory];
}

#pragma mark -
#pragma mark MBLoadPostsOperationDelegate Protocol

- (void)didLoadPosts:(NSArray *)posts  isNext:(BOOL)isNext {
    if (isNext) {
        [self.posts addObjectsFromArray:posts];
    } else {
        self.posts = [NSMutableArray arrayWithArray:posts];
    }
    
    [self.presenter didFetchPosts:self.posts];
}


- (void)didFailToLoadPostsWithIsNext:(BOOL)isNext {
    if (isNext) {
        [self.presenter didFailToFetchNextPosts];
    } else {
        [self.presenter didFailToFetchPosts];
    }
}

@end

