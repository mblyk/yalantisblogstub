//
//  MBPostsListPresenter.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBPostsListViewController.h"
#import "MBPostsListInteractor.h"

@class MBBlogPostTableViewCell;

@interface MBPostsListPresenter : NSObject
@property (nonatomic, weak) MBPostsListViewController *view;
@property (nonatomic, strong)   MBPostsListInteractor *interactor;

- (void)viewDidLoad;
- (void)didScrollToEnd;

- (void)didStartScrolling;
- (void)didStopScrolling;

- (void)didFetchPosts:(NSArray *)posts;
- (void)didFailToFetchPosts;
- (void)didFailToFetchNextPosts;

- (void)willShowCell:(MBBlogPostTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

- (void)didStartPullToRefresh;
- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

- (NSURL *)dataForWebPageModule;

- (NSUInteger)numberOfPosts;

- (void)didReceiveMemoryWarning;

@end
