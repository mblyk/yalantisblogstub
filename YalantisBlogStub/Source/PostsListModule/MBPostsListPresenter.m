//
//  MBPostsListPresenter.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBPostsListPresenter.h"
#import "MBBlogPostTableViewCell.h"
#import "MBPostEntity.h"

@interface MBPostsListPresenter ()
@property (nonatomic, strong)   NSURL *postURL;

@end

@implementation MBPostsListPresenter

#pragma mark -
#pragma mark Initializations

- (instancetype)init {
    self = [super init];
    if (self) {
        self.interactor = [MBPostsListInteractor new];
        self.interactor.presenter = self;
    }
    
    return self;
}

#pragma mark -
#pragma mark Public

- (void)didScrollToEnd {
    [self.interactor fetchNextPosts];
}

- (void)didStartPullToRefresh {
    [self.interactor fetchPosts];
}

- (void)viewDidLoad {
    [self.interactor fetchPosts];
}

- (void)didStartScrolling {
    [self.interactor disableImageLoading];
}

- (void)didStopScrolling {
    [self.interactor enableImageLoadingWithOnScreenIndexPaths:[self.view indexPathsForVisibleRows]];
}

- (void)didFetchPosts:(NSArray *)posts {
    [self.view hidePullToRefresh];
    self.view.isNeedEmptySet = NO;
    [self.view updateData];
}

- (void)didFailToFetchNextPosts {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view hideInfiniteIndicator];
    });
}

- (void)didFailToFetchPosts {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view hidePullToRefresh];
        self.view.isNeedEmptySet = YES;
    });
}

- (NSURL *)dataForWebPageModule {
    return self.postURL;
}

- (void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MBPostEntity *entity = [self.interactor postEntityAtIndexPath:indexPath];
    self.postURL = entity.postURL;
    
    [self.view performSegueWithIdentifier:@"selectCellSegue" sender:self.view];
}

- (void)willShowCell:(MBBlogPostTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [self.interactor setupCell:cell atIndexPath:indexPath];
}

- (NSUInteger)numberOfPosts {
    return [self.interactor numberOfPosts];
}

- (void)didReceiveMemoryWarning {
    [self.interactor clearMemory];
}

@end
