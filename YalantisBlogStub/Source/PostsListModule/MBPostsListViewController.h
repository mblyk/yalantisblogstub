//
//  MBPostsListViewController.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBPostsListPresenter;

@interface MBPostsListViewController : UITableViewController
@property (nonatomic, strong)   MBPostsListPresenter *presenter;

@property (nonatomic, assign)   BOOL isNeedEmptySet;

- (void)updateData;

- (NSArray <NSIndexPath *> *)indexPathsForVisibleRows;

- (void)hidePullToRefresh;
- (void)hideInfiniteIndicator;

@end
