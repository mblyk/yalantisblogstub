//
//  MBPostsListViewController.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBPostsListViewController.h"
#import "MBPostsListPresenter.h"
#import "MBBlogPostTableViewCell.h"
#import "UITableViewCell+MBMBExtension.h"
#import "UIScrollView+INSPullToRefresh.h"
#import "INSLabelPullToRefresh.h"
#import "KeepLayout.h"
#import "MBWebPageViewController.h"
#import "MBWebPagePresenter.h"
#import "UIScrollView+EmptyDataSet.h"

@interface MBPostsListViewController ()<DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end

@implementation MBPostsListViewController


#pragma mark -
#pragma mark Initializations

- (void)awakeFromNib {
    [super awakeFromNib];
    self.presenter = [MBPostsListPresenter new];
    self.presenter.view = self;
}

#pragma mark -
#pragma mark Accessors

- (void)setIsNeedEmptySet:(BOOL)isNeedEmptySet {
    if (_isNeedEmptySet != isNeedEmptySet) {
        _isNeedEmptySet = isNeedEmptySet;
        [self.tableView reloadEmptyDataSet];
    }
}

#pragma mark -
#pragma mark Public

- (void)hideInfiniteIndicator {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView ins_endInfinityScroll];
    });
}

- (void)updateData {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (NSArray<NSIndexPath *> *)indexPathsForVisibleRows {
    return self.tableView.indexPathsForVisibleRows;
}

- (void)hidePullToRefresh {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView ins_endPullToRefresh];
    });
}

#pragma mark -
#pragma ViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setEstimatedRowHeight:200.f];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self configuratePullToRefresh];
    [self configurateInfiniteScroll];
    
    [self.presenter viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = NSLocalizedString(@"blog_view_title", nil); 
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self.presenter didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark TableView Protocols

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBBlogPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[MBBlogPostTableViewCell cellIdentifier]
                                                                    forIndexPath:indexPath];
    
    [self.presenter willShowCell:cell atIndexPath:indexPath];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // push new vC
    [self.presenter didSelectRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter numberOfPosts];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200.f;
}


#pragma mark -
#pragma mark UIScrollViewDelegate Protocol

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self.presenter didStopScrolling];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.presenter didStartScrolling];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.presenter didStopScrolling];
}

#pragma mark -
#pragma mark EmptyDataSet Protocols

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.isNeedEmptySet;
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    if (self.isNeedEmptySet) {
        NSString *text =  NSLocalizedString(@"empty.message", nil);
        
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                     NSForegroundColorAttributeName: [UIColor darkGrayColor]};
        
        return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    }
    
    return nil;
}

#pragma mark -
#pragma mark Private

- (void)configuratePullToRefresh {
    [self.tableView ins_addPullToRefreshWithHeight:60.f handler:^(UIScrollView *scrollView) {
        [self.presenter didStartPullToRefresh];
    }];
    
    UIView <INSPullToRefreshBackgroundViewDelegate> *pullToRefresh = [[INSLabelPullToRefresh alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60.0)
                                                                                                    noneStateText:NSLocalizedString(@"refresh_none", nil)
                                                                                               triggeredStateText:NSLocalizedString(@"refresh_triggered", nil)
                                                                                                 loadingStateText:NSLocalizedString(@"refresh_loading", nil)];
    self.tableView.ins_pullToRefreshBackgroundView.delegate = pullToRefresh;
    [self.tableView.ins_pullToRefreshBackgroundView addSubview:pullToRefresh];
    pullToRefresh.keepLeftInset.equal = 0;
    pullToRefresh.keepBottomInset.equal = 0;
    pullToRefresh.keepRightInset.equal = 0;
    pullToRefresh.keepHeight.equal = 60.f;
}

- (void)configurateInfiniteScroll {
    [self.tableView ins_addInfinityScrollWithHeight:60.f handler:^(UIScrollView *scrollView) {
        [self.presenter didScrollToEnd];
    }];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    
    indicator.color = [UIColor colorWithRed:0.098f green:0.690f blue:0.737f alpha:1.0f];
    
    [indicator startAnimating];
    [self.tableView.ins_infiniteScrollBackgroundView addSubview:indicator];
    
    indicator.keepWidth.equal = 30.f;
    indicator.keepAspectRatio.equal = 1.f;
    [indicator keepCentered];
}

- (void)configurateEmptyDataState {
    
}

#pragma mark -
#pragma mark Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"selectCellSegue"]) {
        MBWebPageViewController *destinationViewController = segue.destinationViewController;
        [destinationViewController.presenter setupPageURL:[self.presenter dataForWebPageModule]];
    }
}


@end
