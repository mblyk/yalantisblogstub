//
//  MBDropdownAlertManager.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBDropdownAlertManager.h"
#import "RKDropdownAlert.h"

@implementation MBDropdownAlertManager

+ (void)showNoConnectionMessage {
    [RKDropdownAlert title:nil
                   message:NSLocalizedString(@"not_reachable", nil)
           backgroundColor:[UIColor  colorWithRed:245.f / 255
                                            green:94.f / 255
                                             blue:78.f / 255
                                            alpha:1.f]
                 textColor:[UIColor whiteColor]
                      time:5];
}

@end
