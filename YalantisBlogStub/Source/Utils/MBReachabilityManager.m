//
//  MBReachabilityManager.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBReachabilityManager.h"
#import "Reachability.h"
#import "MBDropdownAlertManager.h"

@interface MBReachabilityManager ()
@property (nonatomic, strong)   Reachability *reach;

//private methods
- (void)reachabilityChanged:(NSNotification *)object;

@end

@implementation MBReachabilityManager

#pragma mark -
#pragma mark Class Methods

+ (instancetype)sharedManager {
    static MBReachabilityManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    
    return sharedManager;
}

#pragma mark -
#pragma mark Initializations and Deallocation

- (instancetype)init {
    if (self) {
        self.reach = [Reachability reachabilityForInternetConnection];
          self.reach.unreachableBlock = ^(Reachability * reachability){
              dispatch_async(dispatch_get_main_queue(), ^{
                  [MBDropdownAlertManager showNoConnectionMessage];
              });
         };
        
         [self.reach startNotifier];
    }
    
    return self;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark Private

- (void)reachabilityChanged:(NSNotification *)object {
    Reachability *reach = object.object;
    if (!reach.isReachable) {
        [MBDropdownAlertManager showNoConnectionMessage];
    }
}

@end
