//
//  MBStubsResponse.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBStubsResponse : NSObject

+ (NSDictionary *)yalantisBlogResponse;

@end
