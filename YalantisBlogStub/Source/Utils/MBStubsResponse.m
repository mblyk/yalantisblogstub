//
//  MBStubsResponse.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBStubsResponse.h"

@implementation MBStubsResponse

+ (NSDictionary *)yalantisBlogResponse {
    
    NSError *error = nil;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Json"
                                                         ofType:@"json"];
    NSData *dataFromFile = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:dataFromFile
                                                         options:kNilOptions
                                                           error:&error];
    if (error != nil) {
        NSLog(@"Error: was not able to load messages.");
        return nil;
    }
    
    return data;
}

@end
