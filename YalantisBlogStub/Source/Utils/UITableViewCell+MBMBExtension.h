//
//  UITableViewCell+MBMBExtension.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (MBMBExtension)

+ (NSString *)cellIdentifier;

@end
