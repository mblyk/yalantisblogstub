//
//  UITableViewCell+MBMBExtension.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 21.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "UITableViewCell+MBMBExtension.h"

@implementation UITableViewCell (MBMBExtension)

+ (NSString *)cellIdentifier {
    return NSStringFromClass([self class]);
}

@end
