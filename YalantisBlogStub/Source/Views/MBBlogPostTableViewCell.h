//
//  MBBlogPostTableViewCell.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <UIKit/UIKit.h>

@class MBPostEntity;

@interface MBBlogPostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UILabel *postTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *postDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *visitorsCountLabel;

- (void)setupWithPostEntity:(MBPostEntity *)post;
- (void)setupImage:(UIImage *)image;

@end
