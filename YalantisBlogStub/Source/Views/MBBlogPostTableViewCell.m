//
//  MBBlogPostTableViewCell.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 20.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBBlogPostTableViewCell.h"
#import "MBPostEntity.h"

@implementation MBBlogPostTableViewCell

#pragma mark -
#pragma mark Initializations

- (void)awakeFromNib {
    self.postTitleLabel.numberOfLines = 0;
    self.postDescriptionLabel.numberOfLines = 0;
}

#pragma mark -
#pragma mark Public

- (void)prepareForReuse {
    self.postImageView.image = nil;
    self.postTitleLabel.text = nil;
    self.postDescriptionLabel.text = nil;
}

- (void)setupWithPostEntity:(MBPostEntity *)post {
    self.postTitleLabel.text = post.title;
    self.postDescriptionLabel.text = post.postDescription;
    self.authorNameLabel.text = post.author;
    self.visitorsCountLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)post.visitorsCount];
}

- (void)setupImage:(UIImage *)image {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.postImageView.image = image;
    });
}

@end
