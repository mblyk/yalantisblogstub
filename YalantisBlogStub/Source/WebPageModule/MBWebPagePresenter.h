//
//  MBWebPagePresenter.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 22.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBWebPageViewController.h"

@interface MBWebPagePresenter : NSObject
@property (nonatomic, weak)   MBWebPageViewController *view;

- (void)setupPageURL:(NSURL *)pageURL;

- (void)viewWillAppear;

@end
