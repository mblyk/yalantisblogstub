//
//  MBWebPagePresenter.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 22.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBWebPagePresenter.h"

@interface MBWebPagePresenter ()
@property (nonatomic, strong)   NSURL *pageURL;
@end

@implementation MBWebPagePresenter

#pragma mark -
#pragma mark Public

- (void)setupPageURL:(NSURL *)pageURL {
    self.pageURL = pageURL;
}

- (void)viewWillAppear {
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:self.pageURL];
    [self.view loadRequest:request];
}

@end
