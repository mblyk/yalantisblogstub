//
//  MBWebPageViewController.h
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 22.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import <UIKit/UIKit.h>
@class MBWebPagePresenter;

@interface MBWebPageViewController : UIViewController
@property (nonatomic,strong)    MBWebPagePresenter *presenter;

- (void)loadRequest:(NSURLRequest *)request;

@end
