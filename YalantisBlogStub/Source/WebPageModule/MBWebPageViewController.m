//
//  MBWebPageViewController.m
//  YalantisBlogStub
//
//  Created by Mykola Blyk  on 22.02.16.
//  Copyright © 2016 Mykola Blyk . All rights reserved.
//

#import "MBWebPageViewController.h"
#import "KeepLayout.h"
#import <WebKit/WebKit.h>
#import "MBWebPagePresenter.h"

@interface MBWebPageViewController ()
@property (nonatomic, strong)   WKWebView *webView;

@end

@implementation MBWebPageViewController

#pragma mark -
#pragma mark Initializations

- (void)awakeFromNib {
    [super awakeFromNib];
    self.presenter = [MBWebPagePresenter new];
    self.presenter.view = self;
}

#pragma mark -
#pragma ViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView = [WKWebView new];
    [self.view addSubview:self.webView];
    self.webView.keepTopInset.equal = 0;
    self.webView.keepLeftInset.equal = 0;
    self.webView.keepRightInset.equal = 0;
    self.webView.keepBottomInset.equal = 0;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.presenter viewWillAppear];
}

#pragma mark -
#pragma mark Public

- (void)loadRequest:(NSURLRequest *)request {
    [self.webView loadRequest:request];
}

@end
